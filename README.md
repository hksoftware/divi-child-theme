

## Development
This project uses Laravel Mix, Browsersync

### Initial Set up 
Edit webpack.mix.js and set proxy address to your development hostname.

### Auto SCSS/JS compilation
Run `yarn watch` to monitor file changes and compile SCSS/JS as necessary and reload browser


### 
`rm -rf ./node_modules/ && yarn install`

## Deployment
When deploying the theme to a site, the following post-deployment commands must be executed

`yarn install`
`yarn run prod`
