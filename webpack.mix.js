const mix = require('laravel-mix');
const webpack = require('webpack');


mix.js('assets/js/app.js', 'dist/js')
	.sass('assets/sass/app.scss', 'dist/css')
	.sourceMaps(mix.inProduction(), 'source-map')
	.options({ processCssUrls: false })
	.browserSync({
		proxy: 'localhost',
		files: ['dist/**/*', '*/**.php'],
	})
// 	.webpackConfig({
//     resolve: {
//       alias: {
//       },
//     },
//   });
	.webpackConfig({
	module: {
	  rules: [
	  ],
	},
	plugins: [
	  //new StyleLintPlugin({ lintDirtyModulesOnly: !mix.inProduction() }),
	  new webpack.ProvidePlugin({
	    Util: 'exports-loader?Util!bootstrap/js/dist/util',
	  }),
	],
	resolve: {
	  alias: {
	    jquery: 'jquery/src/jquery',
	    waypoints: 'waypoints/lib/jquery.waypoints.js'
	  },
	},
	});