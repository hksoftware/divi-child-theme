<?php

function my_custom_mime_types( $mimes ) {
 
	$extraMimes = [
		'svg' 	=> 'image/svg+xml',
		'svgz' 		=> 'image/svg+xml',
		'doc' 		=> 'application/msword',
		'ttf' 		=> 'font/ttf',
		'otf' 		=> 'font/otf',
		'woff' 		=> 'font/woff'
	];
	// Optional. Remove a mime type.
	unset( $mimes['exe'] );

	$mimes = array_merge($mimes, $extraMimes);
	 
	return $mimes;
}
add_filter( 'upload_mimes', 'my_custom_mime_types' );