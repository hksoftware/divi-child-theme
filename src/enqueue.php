<?php

function custom_enqueue_styles() { 
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

    wp_enqueue_style( 'custom-style', get_stylesheet_directory_uri() . '/dist/css/app.css' );

    wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/dist/js/app.js', [], '1.0.0', true );

	//wp_enqueue_script( 'custom-script-dev', get_stylesheet_directory_uri() . '/dist/js/dev.js', [], '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'custom_enqueue_styles' );
