


class Functions  {

	_CONTENT;
		// Current sentence being processed
	_PART = 0;
	
	// Character number of the current sentence being processed 
	_PART_INDEX = 0;
	
	// Holds the handle returned from setInterval
	_INTERVAL_VAL;
	
	// Element that holds the text
	_ELEMENT;
	
	// Cursor element 
	_CURSOR;


	constructor(titles, textSelector, caretSelector){
		this._CONTENT = titles;
		this._ELEMENT = document.querySelector(textSelector);
		this._CURSOR = document.querySelector(caretSelector);

	}

	animate() {
		// Start the typing effect on load
		this._INTERVAL_VAL = setInterval( () => this.animateHeading() , 100);
	}



	// Implements typing effect
	animateHeading() { 
		// Get substring with 1 characater added
		var text =  this._CONTENT[this._PART].substring(0, this._PART_INDEX + 1);
		
		this._ELEMENT.innerHTML = "&nbsp;" + text;
		this._PART_INDEX++;

		// If full sentence has been displayed then start to delete the sentence after some time
		if(text === this._CONTENT[this._PART]) {
			// Hide the cursor
			//this._CURSOR.style.display = 'none';

			clearInterval(this._INTERVAL_VAL);
			setTimeout(() => {
				const thisClass = this;
				this._INTERVAL_VAL = setInterval( () => {thisClass.deleteText()} , 50);
			}, text.length * 100);
		}
	}

	// Implements deleting effect
	deleteText() {
		// Get substring with 1 characater deleted
		var text =  this._CONTENT[this._PART].substring(0, this._PART_INDEX - 1);
		this._ELEMENT.innerHTML = "&nbsp;" + text;
		this._PART_INDEX--;

		// If sentence has been deleted then start to display the next sentence
		if(text === '') {
			clearInterval(this._INTERVAL_VAL);

			// If current sentence was last then display the first one, else move to the next
			if(this._PART == (this._CONTENT.length - 1))
				this._PART = 0;
			else
				this._PART++;
			
			this._PART_INDEX = 0;

			// Start to display the next sentence after some time
			setTimeout(() => {
				
				const thisClass = this;
				this._CURSOR.style.display = 'inline-block';
				thisClass.animate()
				//this._INTERVAL_VAL = setInterval(thisClass.animate(),100);
			}, 200);
		}
	}

}



module.exports = Functions;