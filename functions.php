<?php

include_once('src/custom-posts.php');
include_once('src/sidebars.php');
include_once('src/shortcodes.php');
include_once('src/enqueue.php');
include_once('src/filters.php');
include_once('src/helpers.php');



/* Stop login from shaking when wrong password entered */
function custom_remove_login_shake() {
    remove_action('login_head', 'wp_shake_js', 12);
}
add_action('login_head', 'custom_remove_login_shake');

define( 'DISALLOW_FILE_EDIT', true );
//define( 'DISALLOW_FILE_MODS', true );// Prevents plugin install